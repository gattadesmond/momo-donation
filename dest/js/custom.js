function navbarFixed() {
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll) {
      $(".navbar-lacxi").addClass("is-fixed");
    } else {
      $(".navbar-lacxi").removeClass("is-fixed");
    }
  });
  $(window).scroll();
}
navbarFixed();
$(".navbar-lacxi").on("click", function () {
  $(".navbar-collapse").collapse("hide");
});

var userAgent = navigator.userAgent || navigator.vendor || window.opera;
if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
  document.body.classList.add("ios");
}

$(window).on("load", function () {
    var hash = window.location.hash;
    var scroll = new SmoothScroll();
    if(hash == "") return;
    var anchor = document.querySelector(hash);
    if (anchor) {
      scroll.animateScroll(anchor);
    }
});

var headerHideAuto = function () {
  var mainHeader = $(".navbar-lacxi");
  var scrolling = false,
    previousTop = 0,
    currentTop = 0,
    scrollDelta = 10,
    scrollOffset = 150;

  function autoHideHeader() {
    var currentTop = $("html").scrollTop();
    if (previousTop - currentTop > scrollDelta) {
      mainHeader.removeClass("is-hidden");
    } else if (
      currentTop - previousTop > scrollDelta &&
      currentTop > scrollOffset
    ) {
      mainHeader.addClass("is-hidden");
    }

    previousTop = currentTop;
    scrolling = false;
  }

  $(window).on("scroll", function () {
    if (!scrolling) {
      scrolling = true;
      !window.requestAnimationFrame
        ? setTimeout(autoHideHeader, 250)
        : requestAnimationFrame(autoHideHeader);
    }
  });
};
headerHideAuto();

// Caching some stuff..
const body = document.body;
const docEl = document.documentElement;

$(".manual-device").each(function (index, element) {
  var $this = $(this);
  var numSlider = $this.attr("childId");
  var device = null;
  var process = null;

  if (numSlider != undefined) {
    device = $this.find(".manual-device-swiper#hd-sub-swp-" + numSlider);
    process = $this
      .parent()
      .parent()
      .find(".manual-process#hd-sub-ctn-" + numSlider);
  } else {
    numSlider = index;
    device = $this.find(".manual-device-swiper");

    process = $this.parent().parent().find(".manual-process ");
  }

  var swiperArray = [];

  swiperArray[numSlider] = new Swiper(device, {
    observer: true,
    observeParents: true,
    on: {
      slideChangeTransitionEnd: function slideChangeTransitionEnd() {
        var num = this.activeIndex;
        process.children(".process_item").removeClass("active");
        process.children(".process_item").eq(num).addClass("active");
      },
    },
  });

  process.children(".process_item").each(function (index, element) {
    var num = index;
    $(element).on("click", function (e) {
      e.preventDefault();
      $(this).addClass("active").siblings().removeClass("active");
      swiperArray[numSlider].slideTo(index);
    });
  });
});

$(".process__body-content").each(function (index, element) {
  var $this = $(this);
  var height = $this[0].scrollHeight;
  var getHeight = height == "0" ? "auto" : height + "px";
  $this.css("--max-height", getHeight);
});


//DONATION HERE

